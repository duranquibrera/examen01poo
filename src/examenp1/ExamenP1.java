/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenp1;
//Ya quedo
/**
 *
 * @author Javier Duran
 */
public class ExamenP1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Recibo recibo = new Recibo();
        
        recibo.setNumRecibo(102);
        recibo.setFecha("21 Marzo 2019");
        recibo.setNombre("Jose Lopez");
        recibo.setDomicilio("Av del Sol 1200");
        recibo.setTipoServicio(1);
        recibo.setCostoKw(2.00f);
        recibo.setKwCons(450);
        
        // Mostrar información 
        System.out.println("Recibo de Luz:");
        System.out.println("Fecha : " + recibo.getFecha());
        System.out.println("Num. Recibo : " + recibo.getNumRecibo());
        System.out.println("Nombre : " + recibo.getNombre());
        System.out.println("Domicilio : " + recibo.getDomicilio());
        System.out.println("Tipo de Servicio : " + recibo.getTipoServicio());
        System.out.println("Costo por Kilowatts : " + recibo.getCostoKw());
        System.out.println("Kilowatts Consumidos : " + recibo.getKwCons());
        
        System.out.println("Sub Total : " + recibo.calcularSubto());
        System.out.println("Impuesto : " + recibo.calcularImpuesto());
        System.out.println("Total Pagar : " + recibo.calcularTotal());
        
        System.out.println("Cotización por Asignacion y Copia:");
        Recibo rec = new Recibo (103,"21 Marzo 2019","Abarrotes feliz","Av del Sol",2,3.00f,1200);
        
        System.out.println("Recibo de Luz:");
        System.out.println("Fecha : " + rec.getFecha());
        System.out.println("Num. Recibo : " + rec.getNumRecibo());
        System.out.println("Nombre : " + rec.getNombre());
        System.out.println("Domicilio : " + rec.getDomicilio());
        System.out.println("Tipo de Servicio : " + rec.getTipoServicio());
        System.out.println("Costo por Kilowatts : " + rec.getCostoKw());
        System.out.println("Kilowatts Consumidos : " + rec.getKwCons());
        
        Recibo rec2 = new Recibo (rec);
        
        System.out.println("Sub Total : " + rec2.calcularSubto());
        System.out.println("Impuesto : " + rec2.calcularImpuesto());
        System.out.println("Total Pagar : " + rec2.calcularTotal());
        
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenp1;

/**
 *
 * @author Javier Duran
 */
public class Recibo {
    
    private int numRecibo;
    private String fecha;
    private String nombre;
    private String domicilio;
    private int tipoServicio;
    private float costoKw;
    private int kwCons;

//Metodos constructores
//Por Omision

public Recibo(){
    this.numRecibo = 0;
    this.fecha = "";
    this.nombre = "";
    this.domicilio = "";
    this.tipoServicio = 0; 
    this.costoKw = 0.0f;
    this.kwCons = 0;
}

public Recibo(int numRecibo,String fecha, String nombre, String domicilio, int tipoServicio, float costoKw, int kwCons){
    this.numRecibo = numRecibo;
    this.fecha = fecha;
    this.nombre = nombre;
    this.domicilio = domicilio;
    this.tipoServicio = tipoServicio; 
    this.costoKw = costoKw;
    this.kwCons = kwCons;
    }

//Copia

public Recibo(Recibo otro){
    this.numRecibo = otro.numRecibo;
    this.fecha = otro.fecha;
    this.nombre = otro.nombre;
    this.domicilio = otro.domicilio;
    this.tipoServicio = otro.tipoServicio; 
    this.costoKw = otro.costoKw;
    this.kwCons = otro.kwCons;
}

    //Metodo get/set

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(int tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public float getCostoKw() {
        return costoKw;
    }

    public void setCostoKw(float costoKw) {
        this.costoKw = costoKw;
    }

    public int getKwCons() {
        return kwCons;
    }

    public void setKwCons(int kwCons) {
        this.kwCons = kwCons;
    }
    
    public float calcularSubto (){
        float subto = 0.0f;
        subto = kwCons * costoKw;
        return subto;
    }
    
    public float calcularImpuesto(){
        float impuesto = 0.0f;
        impuesto = calcularSubto() * 0.16f;
        return impuesto;
    }
    
    public float calcularTotal(){
        float total = 0.0f;
        total = calcularSubto() + calcularImpuesto();
        return total;
    }
    
}
